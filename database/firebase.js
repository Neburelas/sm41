import firebase from "firebase";
import "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCY_-pY_DvkEfbGDY3inmcTzCHlFntwcAw",
    authDomain: "sm41-785cb.firebaseapp.com",
    databaseURL: "https://sm41-785cb.firebaseio.com",
    projectId: "sm41-785cb",
    storageBucket: "sm41-785cb.appspot.com",
    messagingSenderId: "1073891580522",
    appId: "1:1073891580522:web:bcf88cbcb290a966784813"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore();

  export default {
      firebase,
      db
  };